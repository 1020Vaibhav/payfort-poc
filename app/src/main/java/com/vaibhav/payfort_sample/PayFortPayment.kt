package com.vaibhav.payfort_sample

import android.app.Activity
import android.text.TextUtils
import android.util.Log
import androidx.lifecycle.LifecycleCoroutineScope
import com.codepath.asynchttpclient.AsyncHttpClient
import com.codepath.asynchttpclient.callback.JsonHttpResponseHandler
import com.google.gson.Gson
import com.payfort.fort.android.sdk.base.FortSdk
import com.payfort.fort.android.sdk.base.callbacks.FortCallBackManager
import com.payfort.sdk.android.dependancies.base.FortInterfaces
import com.payfort.sdk.android.dependancies.models.FortRequest
import okhttp3.Headers
import org.json.JSONObject
import java.util.*


class PayFortPaymentUsingSDK(
    private val mContext: Activity,
    private val coroutineScope: LifecycleCoroutineScope,
    val fortCallback: FortCallBackManager?,
    val iPaymentCallBack: IPaymentRequestCallBack,
    private val deviceId: String,
    private val gson: Gson = Gson()
) {


    private var sdkToken: String? = null
    private var payFortData: PayFortDataRequestHolder? = null


    fun requestForPayment(payFortData: PayFortDataRequestHolder) {
        this.payFortData = payFortData

        coroutineScope.executeAsyncTask(
            onPostExecute = {},
            onPreExecute = {},
            doInBackground = {
                val client = AsyncHttpClient()
                client.post(
                    Constant.WS_GET_TOKEN,
                    getTokenParams(),
                    object : JsonHttpResponseHandler() {
                        override fun onFailure(
                            statusCode: Int,
                            headers: Headers?,
                            response: String?,
                            throwable: Throwable?
                        ) {
                            Log.e("Status Code", statusCode.toString())
                            Log.e("Authorization response", response.toString())
                            throwable?.printStackTrace()
                            iPaymentCallBack.onPaymentRequestResponse(Constant.RESPONSE_GET_TOKEN, null)
                        }

                        override fun onSuccess(statusCode: Int, headers: Headers?, json: JSON?) {
                            Log.e("Authorization response", json.toString())
                            Log.e("Status Code", statusCode.toString())

                            val payFortAuthorizationResponseData: PayFortDataResponse =
                                gson.fromJson(
                                    json?.jsonObject.toString(),
                                    PayFortDataResponse::class.java
                                )

                            if (!TextUtils.isEmpty(payFortAuthorizationResponseData.sdkToken)) {
                                sdkToken = payFortAuthorizationResponseData.sdkToken
                                requestPurchase()
                            } else {
                                iPaymentCallBack.onPaymentRequestResponse(
                                    Constant.RESPONSE_GET_TOKEN,
                                    payFortAuthorizationResponseData
                                )
                            }
                        }
                    })

            })
    }

    private fun requestPurchase() {
        Log.e("requestPurchase", "Can initiate a purchase")
        Log.e("requestPurchase", "Initiating a purchase using SDK")
        FortSdk.getInstance().registerCallback(
            mContext,
            getPurchaseFortRequest(),
            FortSdk.ENVIRONMENT.TEST,
            Constant.RESPONSE_PURCHASE,
            fortCallback,
            true,
            object : FortInterfaces.OnTnxProcessed {
                override fun onCancel(
                    requestParamsMap: MutableMap<String, Any>?,
                    responseMap: Map<String, Any>
                ) {
                    val response = JSONObject(responseMap)
                    val payFortData: PayFortDataResponse =
                        gson.fromJson(response.toString(), PayFortDataResponse::class.java)
                    //payFortData.paymentResponse = response.toString()
                    Log.e("Cancel Response", response.toString())
                    if (iPaymentCallBack != null) {
                        iPaymentCallBack.onPaymentRequestResponse(
                            Constant.RESPONSE_PURCHASE_CANCEL,
                            payFortData
                        )
                    }
                }

                override fun onSuccess(
                    requestParamsMap: MutableMap<String, Any>?,
                    fortResponseMap: Map<String, Any>
                ) {
                    val response = JSONObject(fortResponseMap)
                    val payFortData: PayFortDataResponse =
                        gson.fromJson(response.toString(), PayFortDataResponse::class.java)
                    //payFortData.paymentResponse = response.toString()
                    Log.e("Success Response", response.toString())
                    if (iPaymentCallBack != null) {
                        iPaymentCallBack.onPaymentRequestResponse(
                            Constant.RESPONSE_PURCHASE_SUCCESS,
                            payFortData
                        )
                    }
                }

                override fun onFailure(
                    requestParamsMap: MutableMap<String, Any>?,
                    fortResponseMap: Map<String, Any>
                ) {
                    val response = JSONObject(fortResponseMap)
                    val payFortData: PayFortDataResponse =
                        gson.fromJson(response.toString(), PayFortDataResponse::class.java)
                    //payFortData.paymentResponse = response.toString()
                    Log.e("Failure Response", response.toString())
                    if (iPaymentCallBack != null) {
                        iPaymentCallBack.onPaymentRequestResponse(
                            Constant.RESPONSE_PURCHASE_FAILURE,
                            payFortData
                        )
                    }
                }
            })
    }


    private fun getPurchaseFortRequest(): FortRequest? {
        val fortRequest = FortRequest()
        return sdkToken?.takeIf { !sdkToken.isNullOrEmpty() && payFortData != null }?.let {
            val purchaseRequest = PayFortDataRequest(
                customerName = "Vaibhav Barad",
                customerIp = getIPAddress(true),
                customerEmail = payFortData!!.customerEmail,
                command = payFortData!!.command,
                amount = payFortData!!.amount,
                currency = payFortData!!.currency,
                language = payFortData!!.language,
                merchantReference = payFortData!!.merchantReference,
                sdkToken = sdkToken!!,
                tokenName = "f256e38d8e9c4bd6b7088e83e14be6fe"
            )
            fortRequest.requestMap = gson.fromJson<MutableMap<String, Any>>(
                Gson().toJson(purchaseRequest),
                MutableMap::class.java
            )
            return@let fortRequest
        }
    }

    private fun getTokenParams(): String {
        val requestMap = sortedMapOf<String, Any>()
        requestMap[Constant.KEY_SERVICE_COMMAND] = Constant.SDK_TOKEN
        requestMap[Constant.KEY_ACCESS_CODE] = Constant.ACCESS_CODE
        requestMap[Constant.KEY_DEVICE_ID] = deviceId
        requestMap[Constant.KEY_LANGUAGE] = Constant.LANGUAGE_TYPE
        requestMap[Constant.KEY_MERCHANT_IDENTIFIER] = Constant.MERCHANT_IDENTIFIER

        val signature = calculateSignature(requestMap)

        val payFortAuthorizationData = PayFortAuthorizationData(
            serviceCommand = Constant.SDK_TOKEN,
            merchantIdentifier = Constant.MERCHANT_IDENTIFIER,
            accessCode = Constant.ACCESS_CODE,
            signature = signature,
            deviceId = deviceId,
            language = Constant.LANGUAGE_TYPE
        )

        return Gson().toJson(payFortAuthorizationData)
    }





}

