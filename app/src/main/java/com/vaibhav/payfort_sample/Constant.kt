package com.vaibhav.payfort_sample

class Constant {
    companion object {

        val RESPONSE_GET_TOKEN_SUCCESS = 666

        //Request key for response
        val RESPONSE_GET_TOKEN = 111
        val RESPONSE_PURCHASE = 222
        val RESPONSE_PURCHASE_CANCEL = 333
        val RESPONSE_PURCHASE_SUCCESS = 444
        val RESPONSE_PURCHASE_FAILURE = 555

        //WS params
        val KEY_MERCHANT_IDENTIFIER = "merchant_identifier"
        val KEY_SERVICE_COMMAND = "service_command"
        val KEY_DEVICE_ID = "device_id"
        val KEY_LANGUAGE = "language"
        val KEY_ACCESS_CODE = "access_code"
        val KEY_SIGNATURE = "signature"

        //Commands
        val AUTHORIZATION = "AUTHORIZATION"
        val PURCHASE = "PURCHASE"
        val SDK_TOKEN = "SDK_TOKEN"
        val TOKENIZATION = "TOKENIZATION"

        //Test token url
        private val TEST_TOKEN_URL = "https://sbpaymentservices.payfort.com/FortAPI/paymentApi"

        //Live token url
        val LIVE_TOKEN_URL = "https://paymentservices.payfort.com/FortAPI/paymentApi"

        //Make a change for live or test token url from WS_GET_TOKEN variable
        val WS_GET_TOKEN = TEST_TOKEN_URL

        //Statics
        val MERCHANT_IDENTIFIER = "3d2f8581"
        val ACCESS_CODE = "7ezwQdYaiACbBm8dZ96p"
        internal val SHA_REQUEST_PHRASE = "37p.Fo7dnxLeS/MpK5N34W@]"
        val SHA_RESPONSE_PHRASE = "30XkSe5f.UHnWot9qqCD4K}["

        val SHA_TYPE = "SHA-256"
        val CURRENCY_TYPE = "SAR"
        val LANGUAGE_TYPE = "en" //Arabic - ar //English - en

        val JAVASCRIPT_OBJ = "testFunction"
    }
}