package com.vaibhav.payfort_sample

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.payfort.fort.android.sdk.base.FortSdk
import com.payfort.fort.android.sdk.base.callbacks.FortCallBackManager
import com.vaibhav.payfort_sample.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity(), IPaymentRequestCallBack {

    private lateinit var mBinding: ActivityMainBinding

    var fortCallback: FortCallBackManager? = null
    lateinit var deviceId: String
    private var isUsingSDK = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        initilizePayFortSDK();
        setListeners();
    }

    private fun setListeners() {
        with(mBinding) {
            tvPurchase.setOnClickListener {
                if (validateSDK())
                    isUsingSDK = true
                requestForPayfortPayment();
            }
            tvPurchase2.setOnClickListener {
                if (validateSDK())
                    startActivity(Intent(this@MainActivity, CustomPaymentActivity::class.java).apply { putExtra("device_id", deviceId) })
                //finish()
            }
        }

    }

    private fun requestForPayfortPayment() {
        val payFortData = PayFortDataRequestHolder(
            amount = (mBinding.etAmount.text.toString().toInt() * 100).toString(),
            command = Constant.PURCHASE,
            currency = Constant.CURRENCY_TYPE,
            customerEmail = "vaibhav@gmail.com",
            language = Constant.LANGUAGE_TYPE,
            merchantReference = System.currentTimeMillis().toString()
        )
        val payFortPayment =
            PayFortPaymentUsingSDK(this, lifecycleScope, fortCallback, this, deviceId)
        payFortPayment.requestForPayment(payFortData)
    }

    override fun onPaymentRequestResponse(responseType: Int, responseData: PayFortDataResponse?) {
        when (responseType) {
            Constant.RESPONSE_GET_TOKEN -> {
                "Token not generated".showToast(this@MainActivity)
                Log.e("onPaymentResponse", "Token not generated");
            }
            Constant.RESPONSE_PURCHASE_CANCEL -> {
                "Payment cancelled".showToast(this@MainActivity)
                Log.e("onPaymentResponse", "Payment cancelled");
            }
            Constant.RESPONSE_PURCHASE_FAILURE -> {
                "Payment failed".showToast(this@MainActivity)
                Log.e("onPaymentResponse", "Payment failed");
            }
            else -> {
                "Payment successful".showToast(this@MainActivity)
                Log.e("onPaymentResponse", "Payment successful");
            }
        }
    }


    private fun validateSDK(): Boolean {
        when {
            TextUtils.isEmpty(mBinding.etAmount.text.toString()) -> {
                "Enter Amount".showToast(this@MainActivity)
                return false
            }
            Constant.MERCHANT_IDENTIFIER == "" -> {
                "Please add MERCHANT_IDENTIFIER in class: " + PayFortPaymentUsingSDK::class.java.simpleName.showToast(
                    this@MainActivity
                )
                return false
            }
            Constant.ACCESS_CODE == "" -> {
                "Please add ACCESS_CODE in class: " + PayFortPaymentUsingSDK::class.java.simpleName.showToast(
                    this@MainActivity
                )
                return false
            }
            Constant.SHA_RESPONSE_PHRASE == "" -> {
                "Please add SHA_RESPONSE_PHRASE in class: " + PayFortPaymentUsingSDK::class.java.simpleName.showToast(
                    this@MainActivity
                )
                return false
            }
            Constant.SHA_RESPONSE_PHRASE == "" -> {
                "Please add SHA_RESPONSE_PHRASE in class: " + PayFortPaymentUsingSDK::class.java.simpleName.showToast(
                    this@MainActivity
                )
                return false
            }
            !isNetworkAvailable(this@MainActivity) -> {
                "Please Check your Internet Connection".showToast(this@MainActivity)
                return false
            }
            else -> return true
        }
    }

    private fun initilizePayFortSDK() {
        fortCallback = FortCallBackManager.Factory.create()
        deviceId = FortSdk.getDeviceId(this@MainActivity)
        Log.e("deviceId", deviceId);
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constant.RESPONSE_PURCHASE) {
            fortCallback!!.onActivityResult(requestCode, resultCode, data)
        }
    }
}