package com.vaibhav.payfort_sample

import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.JavascriptInterface
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.lifecycleScope
import com.vaibhav.payfort_sample.databinding.ActivityCustomPaymentBinding
import okhttp3.*
import java.io.IOException
import java.util.*


class CustomPaymentActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityCustomPaymentBinding

    lateinit var deviceId: String
    private val coroutineScope: LifecycleCoroutineScope = lifecycleScope


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityCustomPaymentBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mBinding.progress.visibility = View.VISIBLE

        deviceId = intent.getStringExtra("device_id").toString()
        mBinding.payBtn.setOnClickListener {
            performTokenization()
        }

        mBinding.pageLayoutWV.settings.apply {
            javaScriptEnabled = true
            displayZoomControls = false
        }
        mBinding.pageLayoutWV.apply {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                WebView.setWebContentsDebuggingEnabled(true);
            }
            webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(
                    view: WebView?,
                    request: WebResourceRequest?
                ): Boolean {
                    val uri: Uri = request!!.url
                    view?.loadUrl(uri.toString())
                    return false
                }

                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    mBinding.progress.visibility = View.GONE
                    if(url!!.contains("success"))

                    view?.loadUrl("javascript: " +
                            "window.androidObj.textToAndroid = function(message) { " +Constant.JAVASCRIPT_OBJ + ".fromWeb(message) }")
                }
            }

            addJavascriptInterface(object {
                @JavascriptInterface
                fun fromWeb(fromWeb: String) {
                    Toast.makeText(this@CustomPaymentActivity, "Payment successful", Toast.LENGTH_SHORT)
                        .show()
                    finish()
                }
            }, Constant.JAVASCRIPT_OBJ)

        }
        mBinding.pageLayoutWV.loadUrl("https://payfort.1020dev.com/custom")
    }

    private fun performTokenization() {
        coroutineScope.executeAsyncTask(
            onPostExecute = {},
            onPreExecute = {},
            doInBackground = {
                val client = OkHttpClient()
                val formBody: RequestBody = getTokenParams()

                val request: Request = Request.Builder()
                    .url("https://sbcheckout.PayFort.com/FortAPI/paymentPage")
                    .post(formBody)
                    .build()

                client.newCall(request).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        Log.e("Tokenization request", "Failure")
                        Log.e("Authorization response", call.toString())
                        e.printStackTrace()
                    }

                    override fun onResponse(call: Call, response: Response) {
                        response.use {
                            if (!response.isSuccessful) {
                                Log.e("Tokenization request", "Failure")
                                Log.e("Authorization response", response.toString())
                            }

                            Log.e("Tokenization request", "Success")
                            Log.e("Authorization response", response.toString())
                            println(response.body!!.string())
                        }
                    }
                })
            })
    }

    private fun getTokenParams(): FormBody {
        val requestMap = sortedMapOf<String, Any>()
        requestMap[Constant.KEY_SERVICE_COMMAND] = Constant.TOKENIZATION
        requestMap[Constant.KEY_ACCESS_CODE] = Constant.ACCESS_CODE
        requestMap[Constant.KEY_DEVICE_ID] = deviceId
        requestMap[Constant.KEY_LANGUAGE] = Constant.LANGUAGE_TYPE
        requestMap[Constant.KEY_MERCHANT_IDENTIFIER] = Constant.MERCHANT_IDENTIFIER

        val signature = calculateSignature(requestMap)

        val formBody = FormBody.Builder()
            .add("serviceCommand", Constant.TOKENIZATION)
            .add("merchantIdentifier", Constant.MERCHANT_IDENTIFIER)
            .add("accessCode", Constant.ACCESS_CODE)
            .add("signature", signature)
            .add("deviceId", deviceId)
            .add("language", Constant.LANGUAGE_TYPE)
            .add("merchantReference", System.currentTimeMillis().toString())
            .add("cardHolderName", mBinding.holderNameET.text.toString())
            .add("cardNumber", mBinding.cardNumberET.text.toString())
            .add("expiryDate", mBinding.expiryDateET.text.toString())
            .add("cardSecurity", mBinding.cvvET.text.toString())
            .add("rememberMe", if (mBinding.rememberMeTB.isChecked) "YES" else "NO")
            .add("returnUrl", "https://www.xyz.com")
            .add("currency", "AED")
            .build()

        val payFortTokenizationData = PayFortTokenizationData(
            serviceCommand = Constant.TOKENIZATION,
            merchantIdentifier = Constant.MERCHANT_IDENTIFIER,
            accessCode = Constant.ACCESS_CODE,
            signature = signature,
            deviceId = deviceId,
            language = Constant.LANGUAGE_TYPE,
            merchantReference = System.currentTimeMillis().toString(),
            cardHolderName = mBinding.holderNameET.text.toString(),
            cardNumber = mBinding.cardNumberET.text.toString(),
            expiryDate = mBinding.expiryDateET.text.toString(),
            cardSecurity = mBinding.cvvET.text.toString(),
            rememberMe = if (mBinding.rememberMeTB.isChecked) "YES" else "NO",
            returnUrl = "https://www.xyz.com",
            currency = "AED"
        )
        Log.e("Tokenization request", "formBody ${payFortTokenizationData.toString()}")
        return formBody
    }
}
