package com.vaibhav.payfort_sample

import android.util.Log
import java.util.*

fun calculateSignature(requestMap: SortedMap<String, Any>): String {
    var requestString = Constant.SHA_REQUEST_PHRASE
    for ((key, value) in requestMap) requestString += "$key=$value"
    requestString += Constant.SHA_REQUEST_PHRASE
    Log.e("concatenatedString", requestString)

    val signature = getSignatureSHA256(requestString)
    Log.e("signature", signature)
    return signature
}


