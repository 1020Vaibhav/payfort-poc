package com.vaibhav.payfort_sample

interface IPaymentRequestCallBack {
    fun onPaymentRequestResponse(responseType: Int, responseData: PayFortDataResponse?)
}