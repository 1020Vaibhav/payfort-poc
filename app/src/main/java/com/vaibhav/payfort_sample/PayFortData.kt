package com.vaibhav.payfort_sample

import com.google.gson.annotations.SerializedName
import java.math.BigInteger

data class PayFortDataResponse(
    val command: String,
    @SerializedName("merchant_reference")
    val merchantReference: String,
    val currency: String,
    @SerializedName("customer_email")
    val customerEmail: String,
    @SerializedName("fort_id")
    val fortId: BigInteger,
    @SerializedName("sdk_token")
    val sdkToken: String,
    @SerializedName("token_name")
    val tokenName: String,
    @SerializedName("payment_option")
    val paymentOption: String,
    @SerializedName("eci")
    val eci: String,
    @SerializedName("authorization_code")
    val authorizationCode: String,
    @SerializedName("order_description")
    val order_description: String,
    @SerializedName("response_message")
    val response_message: String,
    @SerializedName("response_code")
    val responseCode: String,
    @SerializedName("customer_ip")
    val customerIp: String,
    @SerializedName("customer_name")
    val customerName: String,
    @SerializedName("expiry_date")
    val expiryDate: String,
    @SerializedName("card_number")
    val cardNumber: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("phone_number")
    val phoneNumber: String,
    @SerializedName("settlement_reference")
    val settlementReference: String,
    @SerializedName("merchant_extra")
    val merchantExtra: String,
    @SerializedName("merchant_extra1")
    val merchantExtra1: String,
    @SerializedName("merchant_extra2")
    val merchantExtra2: String,
    @SerializedName("merchant_extra3")
    val merchantExtra3: String,
    @SerializedName("merchant_extra4")
    val merchantExtra4: String,
    @SerializedName("merchant_extra5")
    val merchantExtra5: String,
)

data class PayFortDataRequestHolder(
    val amount: String,
    val command: String,
    val currency: String,
    val customerEmail: String,
    val language: String,
    @SerializedName("merchant_reference")
    val merchantReference: String
)

data class PayFortDataRequest(
    val command: String,
    @SerializedName("merchant_reference")
    val merchantReference: String,
    val currency: String,
    val amount: String,
    val language: String,
    @SerializedName("customer_email")
    val customerEmail: String,
    @SerializedName("sdk_token")
    val sdkToken: String,
    @SerializedName("token_name")
    val tokenName: String,
    @SerializedName("payment_option")
    val paymentOption: String? = null,
    @SerializedName("eci")
    val eci: String? = null,
    @SerializedName("customer_ip")
    val customerIp: String,
    @SerializedName("customer_name")
    val customerName: String,
    @SerializedName("phone_number")
    val phoneNumber: String? = null,
    @SerializedName("settlement_reference")
    val settlementReference: String? = null,
    @SerializedName("merchant_extra")
    val merchantExtra: String? = null,
    @SerializedName("merchant_extra1")
    val merchantExtra1: String? = null,
    @SerializedName("merchant_extra2")
    val merchantExtra2: String? = null,
    @SerializedName("merchant_extra3")
    val merchantExtra3: String? = null,
    @SerializedName("merchant_extra4")
    val merchantExtra4: String? = null,
    @SerializedName("merchant_extra5")
    val merchantExtra5: String? = null,
)

data class PayFortAuthorizationData(
    @SerializedName("service_command")
    val serviceCommand: String,
    @SerializedName("access_code")
    val accessCode: String,
    @SerializedName("merchant_identifier")
    val merchantIdentifier: String,
    @SerializedName("language")
    val language: String,
    @SerializedName("device_id")
    val deviceId: String,
    @SerializedName("signature")
    val signature: String,
)


data class PayFortTokenizationData(
    @SerializedName("service_command")
    val serviceCommand: String,
    @SerializedName("access_code")
    val accessCode: String,
    @SerializedName("merchant_identifier")
    val merchantIdentifier: String,
    @SerializedName("merchant_reference")
    val merchantReference: String? = null,
    @SerializedName("language")
    val language: String,
    @SerializedName("device_id")
    val deviceId: String,
    @SerializedName("signature")
    val signature: String,

    @SerializedName("expiry_date")
    val expiryDate: String,
    @SerializedName("card_number")
    val cardNumber: String,
    @SerializedName("card_security_code")
    val cardSecurity: String,
    @SerializedName("card_holder_name")
    val cardHolderName: String,
    @SerializedName("remember_me")
    val rememberMe: String,
    @SerializedName("return_url")
    val returnUrl: String,@SerializedName("currency")
    val currency: String,
)

data class PayFortAuthorizationResponseData(
    @SerializedName("service_command")
    val serviceCommand: String,
    @SerializedName("access_code")
    val accessCode: String,
    @SerializedName("merchant_identifier")
    val merchantIdentifier: String,
    @SerializedName("language")
    val language: String,
    @SerializedName("device_id")
    val deviceId: String,
    @SerializedName("signature")
    val signature: String,
    @SerializedName("sdk_token")
    val sdkToken: String,
    @SerializedName("response_message")
    val responseMessage: String,
    @SerializedName("response_code")
    val responseCode: Int,
    @SerializedName("status")
    val status: Int,
)
